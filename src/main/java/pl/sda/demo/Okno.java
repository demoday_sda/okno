package pl.sda.demo;

import javax.swing.*;
import java.awt.*;

public class Okno extends JFrame {
    private JPanel mainPanel;

    public Okno() throws HeadlessException {
        setContentPane(mainPanel);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        setPreferredSize(new Dimension(500, 400));

        pack();
    }
}
